import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import dotenv from 'dotenv'
import colors from 'colors'
import connectDB from './configs/db.js'
import UserRouter from './Routes/user.route.js'

const app = express()
const PORT = process.env.PORT || 5000

// Middleware
app.use(express.json())

// Load environment variables from .env file
dotenv.config()

// Enable CORS for all routes
app.use(cors())

// Log HTTP requests to the console
app.use(morgan('dev'))

// Test route
app.get('/', (req, res) => {
  res.status(200).send('<h1>Welcome to the app!</h1>')
})

// API_ROUTES
app.use('/api/v1', UserRouter)

// Connect to MongoDB
connectDB().then(() => {
  // Start the server
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`.rainbow)
  })
})
