import UserModel from './../Models/user.js'
import bcrypt from 'bcryptjs'

// @desc: To create a new user entry
// @type: POST
// @endpoint: /api/v1/signup
// @secure: false
const register = async (req, res) => {
  const { email, password } = req.body
  // checking if user with this email already exists
  const userExists = await UserModel.findOne({ email })
  if (userExists) {
    return res.status(409).json({ error: 'User already exists' })
  }

  // Hash password
  const salt = await bcrypt.genSalt(10)
  const hashedPassword = await bcrypt.hash(password, salt)

  const newUser = new UserModel({
    ...req.body,
    password: hashedPassword
  })

  try {
    // Save new user to database
    await newUser.save()
    return res.status(201).json({ message: 'User created successfully', user: newUser })
  } catch (err) {
    console.error(err)
    return res.status(500).json({ error: 'Server error' })
  }
}

// @desc: To login an existing user
// @type: POST
// @endpoint: /api/v1/login
// @secure: false
const loginUser = async (req, res) => {
  const { email, password } = req.body

  // Find user in database
  const user = await UserModel.findOne({ email })
  if (!user) {
    return res.status(401).json({ error: 'Invalid email or password' })
  }

  // Check password
  const isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) {
    return res.status(401).json({ error: 'Invalid email or password' })
  }

  // Create JWT token
  //   const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
  //     expiresIn: '1d'
  //   })

  return res
    .status(200)
    .json({
      message: 'User found successfully!',
      user: { _id: user._id, firstName: user.firstName, lastName: user.lastName, email: user.email }
    })
}

export { register, loginUser }
