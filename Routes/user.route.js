import { Router } from 'express'
import { loginUser, register } from '../Controllers/user.controller.js'

const router = Router()

router.post('/signup', register).post('/login', loginUser)

export default router
