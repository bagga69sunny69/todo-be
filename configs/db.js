import mongoose from 'mongoose'
import dotenv from 'dotenv'
import colors from 'colors'

dotenv.config()

const MONGODB_USER = process.env.DB_USER
const MONGODB_KEY = process.env.DB_KEY
const MONGODB_URI = process.env.DB_URI.replace('%USER%', MONGODB_USER).replace('%KEY%', MONGODB_KEY)

const connectDB = async () => {
  try {
    await mongoose.connect(MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    console.log('MongoDB connected!'.bold.underline.cyan)
  } catch (err) {
    console.error(err.message)
    process.exit(1)
  }
}

export default connectDB
